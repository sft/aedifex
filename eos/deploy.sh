#!/usr/bin/env bash

set -e # Fail script on first error
shopt -s nullglob # Don't fail on empty array loops

# Read environment variables from previous build job
source ${WORKSPACE}/properties.txt


# Check environment variables, exit if they are not set
# Expecting values like LCG_VERSION=dev3, BUILD_MODE=nightly, PLATFORM=x86_64-centos7-gcc8-opt
echo
echo "------------------------------------------------------------------------------------------"
echo "LCG_VERSION:  ${LCG_VERSION:?You need to set LCG_VERSION (non-empty)}"
echo "BUILD_MODE:   ${BUILD_MODE:?You need to set BUILD_MODE (non-empty)}"
echo "PLATFORM:     ${PLATFORM:?You need to set PLATFORM (non-empty)}"
echo "WORKSPACE:    ${WORKSPACE:?You need to set PLATFORM (non-empty)}"
echo "WEEKDAY_NAME: ${WEEKDAY_NAME:?You need to set WEEKDAY_NAME (non-empty)}"
echo "------------------------------------------------------------------------------------------"
echo


BUILD_DIR="${WORKSPACE}/build"

EOS_MGM_URL="root://eosuser.cern.ch"
EOS_BASE_DIR="/eos/project/l/lcg/www/lcgpackages/tarFiles"

TARBALLS="tarfiles/*.tgz"
TARBALLS_LIST="tarballs-${PLATFORM}.sh"

# Authenticate with Kerberos
kinit sftnight@CERN.CH -5 -V -k -t /credentials/sftnight.keytab

# Reset internal bash counter for calculation of the duration
SECONDS=0


if [[ ${BUILD_MODE} == "nightly" ]]
then
    echo "[INFO] Deploying NIGHTLY builds to EOS ..."

    # Define EOS target for Nightlies
    EOS_TARGET_DIR="${EOS_BASE_DIR}/nightlies/${LCG_VERSION}/${WEEKDAY_NAME}"

    # Define XRootD command with options for Nightlies
    XRDCP_CMD="xrdcp --force --posc"

    # In order to enable debug prints, set the environment variable DEBUG
    if [[ -n ${DEBUG} ]]
    then
        XRDCP_CMD="${XRDCP_CMD} --verbose"
        echo "[INFO] Enabling debug prints ..."
        set -x
    else
        XRDCP_CMD="${XRDCP_CMD} --silent"
    fi

    # Find all old buildinfo, isDone, isDone-unstable and tarballs on EOS for the current platform
    # The "|| true" part at the end makes sure that the command doesn't fail for zero matches in grep
    OLD_FILES=( "$( xrdfs ${EOS_MGM_URL} ls -1 ${EOS_TARGET_DIR} | grep ${PLATFORM} || true )" )
    for OLD_FILE in ${OLD_FILES[@]}
    do
        echo "[INFO] Deleting file ${OLD_FILE} ..."
        xrdfs ${EOS_MGM_URL} rm ${OLD_FILE}
    done

    # Copy isDonFe to EOS if it exists
    if [[ -f ${BUILD_DIR}/isDone-${PLATFORM} ]]
    then
        echo "[INFO] Deploying isDone-${PLATFORM} ..."
        ${XRDCP_CMD} "${BUILD_DIR}/isDone-${PLATFORM}" "${EOS_MGM_URL}/${EOS_TARGET_DIR}/isDone-${PLATFORM}?eos.atomic=1"
    fi

    # Copy isDone-unstable to EOS if it exists
    if [[ -f ${BUILD_DIR}/isDone-unstable-${PLATFORM} ]]
    then
        echo "[INFO] Deploying isDone-unstable-${PLATFORM} ..."
        ${XRDCP_CMD} "${BUILD_DIR}/isDone-unstable-${PLATFORM}" "${EOS_MGM_URL}/${EOS_TARGET_DIR}/isDone-unstable-${PLATFORM}?eos.atomic=1"
    fi

    # Generate and deploy the 'tarballs' and 'symlinks' build info txt files
    echo "[INFO] Parsing LCG_${LCG_VERSION}_${PLATFORM}.txt ..."
    python3 /aedifex/eos/parse-build-info.py "${LCG_VERSION}" "${PLATFORM}"

    echo "[INFO] Deploying LCG_${LCG_VERSION}_${PLATFORM}_tarballs.txt ..."
    ${XRDCP_CMD} "${BUILD_DIR}/LCG_${LCG_VERSION}_${PLATFORM}_tarballs.txt" "${EOS_MGM_URL}/${EOS_TARGET_DIR}/LCG_${LCG_VERSION}_${PLATFORM}_tarballs.txt?eos.atomic=1"
    
    echo "[INFO] Deploying LCG_${LCG_VERSION}_${PLATFORM}_symlinks.txt ..."
    ${XRDCP_CMD} "${BUILD_DIR}/LCG_${LCG_VERSION}_${PLATFORM}_symlinks.txt" "${EOS_MGM_URL}/${EOS_TARGET_DIR}/LCG_${LCG_VERSION}_${PLATFORM}_symlinks.txt?eos.atomic=1"

    # Copy build info txt file to EOS
    echo "[INFO] Deploying LCG_${LCG_VERSION}_${PLATFORM}.txt ..."
    ${XRDCP_CMD} "${BUILD_DIR}/LCG_${LCG_VERSION}_${PLATFORM}.txt" "${EOS_MGM_URL}/${EOS_TARGET_DIR}/LCG_${LCG_VERSION}_${PLATFORM}.txt?eos.atomic=1"
else
    echo "[INFO] Deploying RELEASE builds to EOS ..."

    # Define EOS target for Releases
    EOS_TARGET_DIR="${EOS_BASE_DIR}/releases"

    # Define XRootD command with options for Releases
    XRDCP_CMD="xrdcp --posc"

    # Copy build info txt file to EOS; Use '--force' here in order to update old releases with new packages
    echo "[INFO] Deploying LCG_${LCG_VERSION}_${PLATFORM}.txt ..."
    ${XRDCP_CMD} --force "${BUILD_DIR}/LCG_${LCG_VERSION}_${PLATFORM}.txt" "${EOS_MGM_URL}/${EOS_TARGET_DIR}/LCG_${LCG_VERSION}_${PLATFORM}.txt?eos.atomic=1"
fi


# Copy tarballs from build directory to EOS
COUNTER=1
TARBALL_COUNT=$( ls -1 ${BUILD_DIR}/${TARBALLS} | wc -l )
for TARBALL in ${BUILD_DIR}/${TARBALLS}
do
    TARBALL_BASENAME="$( basename ${TARBALL} )"
    printf "[INFO] [%03d/%03d] Deploying %s ...\n" ${COUNTER} ${TARBALL_COUNT} ${TARBALL_BASENAME}

    ${XRDCP_CMD} "${TARBALL}" "${EOS_MGM_URL}/${EOS_TARGET_DIR}/${TARBALL_BASENAME}?eos.atomic=1"

    ((COUNTER++))
done


# Reset internal bash counter for calculation of the duration
echo "--------------------------------------------------------------------------------------------"
echo " Deployed files to EOS in $(( SECONDS / 60 )):$(( SECONDS % 60 )) minutes."
echo "--------------------------------------------------------------------------------------------"

# TODO: Deploy tarball list to S3 instead of EOS?

# TODO: Create a list of all symlinks

# TODO Different trigger approach than the ${TARBALLS_LIST} ??

# TODO return codes vs. bash -e vs. retry
