# aedifex - LCG Continuous Integration Scripts

![aedifex logo](aedifex.png)

aedifex is a collection of CMake, Bash and Python scripts that are used by SPI for the LCG releases and nightlies in the continuous integration server Jenkins. It consists of cherry-picked and refactored scripts taken from `sft/lcgjenkins`.

## Breaking changes compared to `lcgjenkins`

Some environment variables were renamed to avoid misunderstandings and double usage:

- `weekday` is now `WEEKDAY_NAME`; possible values are still the same: `Mon`, `Tue`, `Wed`, `Thu`, `Fri`, `Sat`, `Sun`
- `ARCHITECTURE` is now `INSTRUCTION_SET` to reflect its usage for SIMD in Jenkins; we used values like `sse3` or `avx2+fma` for `devgeantv`
- `MODE` is now `CTEST_TRACK` to set the name for the build track in CDash; available slots are for example `Release`, `Experimental`, `dev3`, `dev4`, `dev3python3`, `dev3cuda`, `dev4cuda`, `devgeantv`, `devBE` (These slots cannot be created by SPI, currently it can be done by [Javier Cervantes Villanueva](mailto:javier.cervantes.villanueva@cern.ch))
- The Docker images follow a more standard conform naming scheme: e.g. `gitlab-registry.cern.ch/sft/docker/centos7:latest` instead of `gitlab-registry.cern.ch/sft/docker:lcg-cc7`
- `BUILDTYPE` is now `BUILD_TYPE` and its values changed: Instead of `Release` and `Debug` we use `opt` and `dbg` now to avoid confusion with Relases / Nightlies since the build type only refers to the compilation process.
- `BUILDMODE` is now `BUILD_MODE`; possible values are still the same: `nightly`

Some functionalities were removed because they were never or no longer used and complicated the build scripts by introducing unnecessary special cases. Removed support for ...

- the `/afs/sw` area because the service is discontinued by CERN IT
- the Intel compiler `icc`
- `gcc` older than version 6.1
- `clang` versions older than 5.0.1
- Microsoft Windows as build environment
- ENV `CXXOPTIONS` to define for example `std14` in the platform as used C++ standard because it is not used
- ENV `WORKDIR` because we use `WORKSPACE` everyehere now
- ENV `ExtraCMakeOptions` since it seems to never used (instead `LCG_EXTRA_OPTIONS` is used ever since)
- the lock file `/tmp/the.lock` since we only remove it but never set or read it in the old scripts

## Determining the build platform

One crucial part of the build scripts is to determine the build platform, e.g. `x86_64-centos7-gcc8-opt`. In the old scripts we detect the platform in three places: `lcgjenkins/getPlatform.py`, `lcgjenkins/macros.cmake` and `lcgcmake/heptools-common.cmake`. In order to streamline the detection process, only `aedifex/get_platform.py` is used as a single source of truth. It is based on `Python 3` and its `distro` module which has been incorporated in every build Docker container image. The functionality of `macros.cmake` used to be in `lcgcmake.cmake` but was removed. Now it uses the environment variable `$PLATFORM` that is set to the output of `get_platform.py` after fixing the compiler setup. Since changes in the `lcgcmake` project would be difficult due to side effects with other users, the file `heptools-common.cmake` is left unchanged but by setting the environment variable `$BINARY_TAG` to the same vakue as `$PLATFORM` the internal platform calculation can be overwritten and the usage of the given platform can be enforced. Thus the platform is now only determined in one place and can be changed there for all usages.

## Copy build artifacts to EOS

```bash
BUILDMODE="nightly"
LCG_VERSION="dev3"
PLATFORM="x86_64-ubuntu1804-gcc8-opt"
WORKSPACE_HOST="$( pwd )"
WORKSPACE="/workspace"
docker run --rm -it \
    --env BUILDMODE=${BUILDMODE} \
    --env LCG_VERSION=${LCG_VERSION} \
    --env PLATFORM=${PLATFORM} \
    --env WORKSPACE=${WORKSPACE} \
    --name utility-$( date +%Y%m%d-%H%M%S ) \
    --hostname ${HOSTNAME}-docker \
    --security-opt seccomp:unconfined \
    --volume /ec/conf:/ec/conf \
    --volume /cvmfs:/cvmfs \
    --volume ${WORKSPACE_HOST}/docker:${WORKSPACE} \
    gitlab-registry.cern.ch/sft/docker/utility \
    source /scripts/eos/deploy_to_eos.sh
```

## Copy files from EOS to CVMFS

```bash

```
