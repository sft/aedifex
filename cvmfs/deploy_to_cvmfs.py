#!/usr/bin/env python

import os
import sys
import re
import argparse
import subprocess
import shutil
import requests

def checkURL(url):
    if "file://" in url:
        return os.system('stat {0} 1>/dev/null 2>/dev/null'.format(url.replace('file://', ''))) == 0
        # if stat > /dev/null 2>&1
    ret = requests.head(url)
    return ret.status_code == 200
    # if curl --output /dev/null --silent --head --fail "$url"; then

class Package:
    cache = {}

    def __init__(self, name, version, hashstr, directory, dependencies, platform, compiler):
        self.name = name
        self.version = version
        self.hashstr = hashstr
        self.directory = directory
        self.dependencies = dependencies
        self.platform = platform
        self.compiler = compiler

    def getName(self):
        return "{0}-{1}".format(self.name, self.version)

    def getPackageFilename(self):
        return "{0}-{1}_{2}-{3}.tgz".format(self.name, self.version, self.hashstr, self.platform)

    def getModifiedInstallPath(self):
        return os.path.join(self.directory, "{0}-{1}".format(self.version, self.hashstr), self.platform)

    def getInstallPath(self):
        return os.path.join(self.directory, self.version, self.platform)

class InstallProcess:
    def __init__(self, releaseurl, description, prefix='.', lcgversion='auto',  updatelinks=False, nocheck=False, nightly=False, limited=False, endsystem='cvmfs'):
        self.packages = []
        self.releaseurl = releaseurl
        self.prefix = prefix
        self.basepath = '/cvmfs/sft.cern.ch/lcg/releases'

        if lcgversion != "auto":
            self.lcgversion = lcgversion
        else:
            lcgversion      = description.split("_")[1]
        self.description    = description
        self.platform       = '_'.join(description.replace('.txt', '').split('_')[2:])

        p_arch, p_osvers, p_compvers, p_buildtype = self.platform.split('-')
        self.nakedplatform  = '-'.join([p_arch.split('+')[0], p_osvers, p_compvers, p_buildtype])

        self.Nightly        = nightly
        self.updatelinks    = updatelinks
        self.limited        = limited
        print("Starting Nightly Installation")

        if description != "":

            self.fillPackages(description)

            if not nochPACKAGE_NAMEeck:
                print("Checking all tgz files ...")

                for package in self.packages:
                    filename = os.path.join(self.releaseurl, package.getPackageFilename())
                    if not checkURL(filename):
                        # if self.nightly or "rootext" in self.lcgversion:
                        if self.nightly or self.limited:
                            print("This package does not exist in this configuration, however we continue: ", filename)
                        else :
                            raise RuntimeError("URL not found: " + filename)

    @staticmethod
    def getNakedPlatform(platform):
        arch, osvers, compvers, buildtype = platform.split('-')
        return '-'.join([arch.split('+')[0], osvers, compvers, buildtype])

    def fillPackages(self, description):

        url = str(os.path.join(self.releaseurl, self.description))
        if not checkURL(url):
            raise RuntimeError("URL {0} not found.".format(url))
        p = subprocess.Popen(['curl', '-s', url], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout, stderr = p.communicate()

        if p.returncode == 0:
            text = stdout.strip()
        else:
            print("ERROR:")
            print(stderr.strip())
            raise RuntimeError("Cannot get info from " + url)

        lines = text.split('\n')
        # limited contains a line with PKGS_OK
        self.limited = 'PKGS_OK' in lines[0]

        if self.limited:
            selectedpackages = lines[0].split('PKGS_OK ')[1].split(' ')

        for line in lines:
            # Ignore commented lines
            if line[0] == '#':
                continue

            # d = buildinfo2json.parse( line )
            ############################################################################
            data = {}
            text = text.split(', ')
            for key, value in [ x.split(':') for x in text ]:
                data.update( { key.strip() : value.strip() } )
            # deps
            if 'VERSION' in data:
                if 'DEPENDS' not in data:
                    tmp = data['VERSION'].split(',')
                    data['VERSION'] = tmp[0]
                    data['DEPENDS'] = [ x for x in tmp[1:] if len(x) != 0 ]
                else:
                    data['DEPENDS'] = [ x for x in data['DEPENDS'].split(',') if len(x) != 0 ]

            if 'GITHASH' in data:
                # Remove quotation from git hash
                data['GITHASH'] = data['GITHASH'].replace('"', '').replace("'", '')
            ############################################################################

            # special platform with instruction set
            if 'ISET' not in data:
                platform = self.nakedplatform
            else:
                platform = self.platform

            # if "rootext" in self.lcgversion:
            if self.limited:
                for i in selectedpackages:
                    if i != '{0}-{1}'.format(data['NAME'], data['VERSION']):
                        continue
                    else:
                        if data['NAME'] != data['DESTINATION']:
                            print("# Skip package", data['NAME'], 'as it should be packaged in', data['DESTINATION'])
                            continue
                        p = Package(data['NAME'], data['VERSION'], data['HASH'], data['DIRECTORY'], data['DEPENDS'], platform, data['COMPILER'])
                        self.packages.append(p)
            else:
                if data['NAME'] != data['DESTINATION']:
                # bundled package
                    print("# Skip package", data['NAME'], 'as it should be packaged in', data['DESTINATION'])
                    continue
                p = Package(data['NAME'], data['VERSION'], data['HASH'], data['DIRECTORY'], data['DEPENDS'], platform, data['COMPILER'])
                self.packages.append(p)

    @staticmethod
    def createLinks(frompath, topath, relative=True, updatelinks=False):
        # Destroy current link to create a new one to the new installed package
        # Maintain old pointed package
        if updatelinks and os.path.exists(topath):
            print("  Removing existing link {0}->{1}".format(topath, os.path.realpath(topath)))
            os.unlink(topath)

        # Remove trailing '/'
        if frompath[-1] == '/':
            frompath = frompath[:-1]
        if topath[-1] == '/':
            topath = topath[:-1]
        try:
            print("  Checking that symbolic link {0} exists".format(topath))
            if not os.path.exists(topath):
                if not os.path.exists(os.path.dirname(topath)):
                    os.makedirs(os.path.dirname(topath))
                if relative:
                    frompath = os.path.relpath(frompath, topath)
                    frompath = '/'.join(frompath.split('/')[1:])
                print("  Create symbolic link {0}->{1}".format(topath, frompath))
                os.symlink(frompath, topath)
            else:
                print("  Existing link: {0}->{1}".format(topath, os.path.realpath(topath)))
            return True
        except Exception as e:
            raise RuntimeError("Error during managing symlinks: " + str(e))

    # Template method
    def install(self, package, opts='-xpz', force=False):

        # Get source and destination paths for links
        linkpath = self.prefix + "/" + package.getInstallPath() # PREFIX + PKG_VERSION + PKG_PLATFORM
        datapath = os.path.join(self.basepath, package.getModifiedInstallPath()) # /cvmfs/sft.cern.ch/lcg/releases/${pkg_directory}/${pkg_version}-${pkg_hash}/${pkg_platform}

        unTARdone = False

        # Manage copy of file or creation of links
        if not self.isInstalled(package) or force :
            print("  Extract archive from", os.path.join(self.releaseurl, package.getPackageFilename()))
            returncode = self.unTAR(package, opts)
            unTARdone = True

            prefixinstallpath = os.path.join(self.prefix, package.getInstallPath()) # /cvmfs/sft-nightlies.cern.ch/lcg/nightlies/${LCG_VERSION}/${TODAY}/${pkg_directory}/${pkg_version}/${pkg_platform}
            postinstallfile = os.path.join(prefixinstallpath, '.post-install.sh')

            if os.path.exists(postinstallfile):
                print("  Launch .post-install.sh")

                p = subprocess.Popen(['env', 'INSTALLDIR={0}'.format(self.prefix), 'bash', postinstallfile], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                stdout, stderr = p.communicate()
                if p.returncode == 0:
                    print("  OK.")
                    return True
                else:
                    print("  ERROR:")
                    print(stderr.strip())
                    raise RuntimeError("Error in post-install step")

                #returncode = returncode and self.postinstall(postinstallfile)
        else:
            returncode = True

        # Release installation always creates link from .../release/LCG/pkg -> .../release/pkg
        if (self.nightly and not unTARdone) or (not self.nightly):
                if(os.path.exists(datapath)):
                    returncode = returncode and self.createLinks(datapath, linkpath, updatelinks=self.updatelinks)
                else:
                    if self.limited:
                        # Ignoring the prackage....
                        returncode = True
                    else:
                        raise RuntimeError("Path not exists: " + datapath)

        return returncode

    def unTAR(self, package, opts='-xpz'):
        if self.nightly :
            opts += " -v "
        else:
            # verbose (to keep list of untared files)
            opts += " -v --show-transformed-names --transform 's,/{0}/{2},/{0}-{1}/{2},g'".format(package.version, package.hashstr, package.platform)
        filename = os.path.join(self.releaseurl, package.getPackageFilename())
        p = subprocess.Popen('curl -s {0} | tar {1} --directory={2} --file -'.format(filename, opts, self.prefix), stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
        stdout, stderr = p.communicate()

        # Remove only case not matched by the previous regex expresion (packageName/version-withoutHash)
        unmatched = os.path.join(self.prefix, package.directory, package.version)
        if os.path.exists(unmatched):
            # Check whether it is empty
            otherdirs = os.listdir(unmatched)
            if not otherdirs:
                os.rmdir(unmatched)
            elif otherdirs == ["share"]:
                shutil.rmtree(unmatched)

        if p.returncode == 0:
            print("  File:", filename, "Extracted:", len(stdout.strip().split('\n')), 'files')
            return True
        else:
            print("  ERROR: cannot properly run the following command:")
            print('  curl -s', filename, ' | tar', opts, '-C', self.prefix, '-f', '-')
            print(stderr)
            if len(stdout.strip()) != 0:
                tarprefix = sorted(stdout.strip().split('\n'))[0]
                tarprefix = os.path.join(self.prefix, tarprefix)
                print("  Try to revert changes: rm -rf {0}".format(tarprefix))
                try:
                    if tarprefix != "":
                        shutil.rmtree(tarprefix)
                    print("  FAILED. But installation directory should be clean.")
                except:
                    print("  ERROR: cannot remove " + tarprefix)
                raise RuntimeError(stderr)
            else:
                # if self.nightly  or "rootext" in self.lcgversion:
                if self.nightly or self.limited:
                    print("Nothing has been extracted. Probably file not found. anyway let's move on")
                else :
                    raise RuntimeError("Error during extraction.")

    def isInstalled(self, package):
        if self.nightly:
            installpath = os.path.join(self.basepath, package.getModifiedInstallPath()) # /cvmfs/sft.cern.ch/lcg/releases
        else :
            installpath = os.path.join(self.prefix, package.getModifiedInstallPath()) # /cvmfs/sft-nightlies.cern.ch/lcg/nightlies/${LCG_VERSION}/${TODAY}/
        print("  Checking that {0} exists".format(installpath))
        print("Exists?: ", os.path.exists(installpath))
        print("Read_access?", os.access(installpath, os.R_OK))
        return os.path.exists(installpath)

    def getPackages(self):
        return self.packages

def main():
    parser.add_argument('-f', '--force-install', help="Force untar selected packages", default=[], dest='force', nargs='*')
    parser.add_argument('-n', '--dry-run', help="Be pacific, don't do anything", default=False, action='store_true', dest='dryrun')
    parser.add_argument('-l', '--list', help="Just list packages", default=False, action='store_true', dest='justlist')
    parser.add_argument('--update', help="Force to update existing links", default=False, action='store_true', dest='updatelinks')
    parser.add_argument('-o', '--other', help="Installation of limited amount of packages, to be used by rootext or geantv", default=False, action='store_true', dest='limited')

    # -y
    parser.add_argument('-y', '--nightly', help="Install as nightly", default=False, action='store_true', dest='nightly')
    # -u http://lcgpackages.web.cern.ch/lcgpackages/tarFiles/nightlies/${LCG_VERSION}/${TODAY}
    parser.add_argument('-u', '--release-url', help="URL of release area (use file:// for local targets)", default='http://lcgpackages.cern.ch/tarFiles/releases', dest='releaseurl')
    # -r ${LCG_VERSION}
    parser.add_argument('-r', '--release-number', help="Release number", default='auto', dest='releasever')
    # -d LCG_${LCG_VERSION}_${PLATFORM}.txt
    parser.add_argument('-d', '--description', help="Description name", default='', dest='description')
    # -p /cvmfs/sft-nightlies.cern.ch/lcg/nightlies/${LCG_VERSION}/${TODAY}/
    parser.add_argument('-p', '--prefix', help="Installation prefix", default='.', dest='prefix')
    # -e cvmfs
    parser.add_argument('-e', '--endsystem', help="installation in CVMFS, EOS or AFS", default='CVMFS', dest='endsystem')

    args.prefix = os.path.abspath(args.prefix)

    installation = InstallProcess(args.releaseurl,               # http://lcgpackages.web.cern.ch/lcgpackages/tarFiles/nightlies/${LCG_VERSION}/${TODAY}
                                  args.description,              # LCG_${LCG_VERSION}_${PLATFORM}.txt
                                  args.prefix,                   # /cvmfs/sft-nightlies.cern.ch/lcg/nightlies/${LCG_VERSION}/${TODAY}/
                                  args.releasever,               # ${LCG_VERSION}
                                  updatelinks=args.updatelinks,  # default=False
                                  nocheck=args.justlist,         # default=False
                                  nightly=args.nightly,          # True
                                  limited=args.limited,          # default=False
                                  endsystem=args.endsystem)      # cvmfs
    idx = 1
    for package in installation.getPackages():
        print("[{0:03d} / {1:03d}] Start installation process for ".format(idx, len(installation.getPackages())), package.getName())
        force = package.getName() in args.force
        if force:
            print("  Force reinstalling has been requested")
        installation.install(package, force=force)
        print("Finished.")
        idx += 1

    # GCC installation
    compiler = set([x.compiler for x in installation.packages])
    if len(compiler) == 0:
        raise RuntimeError("No compiler found in release")
    elif len(compiler) > 1:
        print("WARNING: More than one compilers found in release:", compiler)

    compiler = sorted(list(compiler))[-1]
    compilerplatform = list(set([x.platform for x in installation.packages]))[0]
    compilerplatform = '-'.join(compilerplatform.split('-')[:-2])

    # remove architecture part (avx,avx2,fma..)
    compilerplatform = re.sub('\+.*-', '-', compilerplatform)
    compilerversion  = compiler.split()[1].strip()

    if "ubuntu" not in args.description and "clang" not in args.description:
        path = '/cvmfs/sft.cern.ch/lcg/contrib/gcc/'
        if os.path.exists(os.path.join(path, compilerversion)):
            path = os.path.join(path, compilerversion)
        elif os.path.exists(os.path.join(os.path.join(path, '.'.join(compilerversion.split('.')[:-1])))):
            path = os.path.join(os.path.join(path, '.'.join(compilerversion.split('.')[:-1])))

        if os.path.exists(os.path.join(path, compilerplatform)):
            compilerpath = os.path.join(os.path.join(path, compilerplatform))
        else:
            raise RuntimeError("Cannot find compiler in {0}".format(os.path.join(path, compilerplatform)))

        if not os.path.exists(os.path.join(args.prefix, 'gcc', compilerversion, compilerplatform)):
            installation.createLinks(compilerpath, os.path.join(args.prefix, 'gcc', compilerversion, compilerplatform), False)

            if not os.path.exists(os.path.join(args.prefix, 'LCG_' + str(args.releasever), 'gcc', compilerversion, compilerplatform)):
                installation.createLinks(compilerpath, os.path.join(args.prefix, "LCG_" + str(args.releasever), 'gcc', compilerversion, compilerplatform), False)
