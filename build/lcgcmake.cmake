cmake_minimum_required(VERSION 2.8)

#---{ Common Settings }----------------------------------------------------------------------------
list(APPEND CMAKE_CONFIGURATION_TYPES Release Debug RelWithDebInfo MinSizeRel TestRelease Maintainer)

#---{ Utility Macros }-----------------------------------------------------------------------------
function(GET_NCPUS ncpu)
    if(APPLE)
        execute_process(COMMAND /usr/sbin/sysctl hw.ncpu OUTPUT_VARIABLE out RESULT_VARIABLE rc)
        if(NOT rc)
            string(REGEX MATCH "[0-9]+" n ${out})
        endif()
    elseif(UNIX)
        execute_process(COMMAND cat /proc/cpuinfo OUTPUT_VARIABLE out RESULT_VARIABLE rc)
        if(NOT rc)
            string(REGEX MATCHALL "processor" procs ${out})
            list(LENGTH procs n)
        endif()
    endif()

    if(DEFINED n)
        set(${ncpu} ${n} PARENT_SCOPE)
    else()
        set(${ncpu} 1 PARENT_SCOPE)
    endif()
endfunction()

function(GET_HOST host)
    execute_process(COMMAND hostname OUTPUT_VARIABLE h OUTPUT_STRIP_TRAILING_WHITESPACE)
    string(TOLOWER ${h} h)
    set(${host} ${h} PARENT_SCOPE) 
endfunction()  

function(GET_DATE date)
    if(UNIX)
        execute_process(COMMAND "date" "+%d/%m/%Y" OUTPUT_VARIABLE tmp OUTPUT_STRIP_TRAILING_WHITESPACE)
    endif()

    if(tmp)
        string(REGEX REPLACE "(..)/(..)/(....).*" "\\3\\2\\1" tmp ${tmp})
    else()
        message(STATUS "WARNING: Date Not Implemented")
        set(tmp 0000)
    endif()

    set(${date} ${tmp} PARENT_SCOPE)
endfunction()

function(GET_TIME time)
    if(UNIX)
        execute_process(COMMAND "date" "+%H:%M" OUTPUT_VARIABLE tmp OUTPUT_STRIP_TRAILING_WHITESPACE)
    endif()

    if(tmp)
        string(REGEX REPLACE "(..):(..)*" "\\1\\2" tmp ${tmp})
    else()
        message(STATUS "WARNING: Time Not Implemented")
        set(tmp 0000)
    endif()

    set(${time} ${tmp} PARENT_SCOPE)
endfunction()

#---{ Make sure that VERBOSE is OFF to avoid screwing up the build performance }-------------------
unset(ENV{VERBOSE})

#---{ General Configuration }----------------------------------------------------------------------
set(CTEST_BUILD_CONFIGURATION $ENV{CMAKE_BUILD_TYPE})
set(CTEST_CONFIGURATION_TYPE ${CTEST_BUILD_CONFIGURATION})

#---{ Set the source and build directory }---------------------------------------------------------
set(CTEST_BUILD_PREFIX "$ENV{WORKSPACE}")
set(CTEST_SOURCE_DIRECTORY "/lcgcmake")
set(CTEST_BINARY_DIRECTORY "${CTEST_BUILD_PREFIX}/build")

#---{ Set the install directory }------------------------------------------------------------------
if("$ENV{INSTALLDIR}" STREQUAL "")
    # Derive install directory from binary directory if ${INSTALLDIR} is not set 
    set(CTEST_INSTALL_DIRECTORY "${CTEST_BUILD_PREFIX}/install")
else()
    set(CTEST_INSTALL_DIRECTORY "$ENV{INSTALLDIR}")
endif()

#---{ Set the CTEST SITE according to the environment }--------------------------------------------
GET_HOST(host)
set(CTEST_SITE "${host}")

#---{ CDash settings }-----------------------------------------------------------------------------
set(CTEST_PROJECT_NAME       "LCGSoft")
set(CTEST_NIGHTLY_START_TIME "01:00:00 UTC")
set(CTEST_DROP_METHOD        "http")
set(CTEST_DROP_SITE          "cdash.cern.ch")
set(CTEST_DROP_LOCATION      "/submit.php?project=LCGSoft")
set(CTEST_DROP_SITE_CDASH    TRUE)

#---{ Custom CTest settings }----------------------------------------------------------------------
set(CTEST_CUSTOM_MAXIMUM_FAILED_TEST_OUTPUT_SIZE "1000000")
set(CTEST_CUSTOM_MAXIMUM_PASSED_TEST_OUTPUT_SIZE "100000")
set(CTEST_CUSTOM_MAXIMUM_NUMBER_OF_ERRORS        "256")
set(CTEST_TEST_TIMEOUT                           1500)

find_program(CTEST_GIT_COMMAND NAMES git)
set(CTEST_UPDATE_COMMAND ${CTEST_GIT_COMMAND})

if(NOT "$ENV{GIT_HASH_LCGCMAKE}" STREQUAL "")
    set(CTEST_GIT_UPDATE_CUSTOM  ${CTEST_GIT_COMMAND} checkout -f $ENV{GIT_HASH_LCGCMAKE})
endif()

GET_NCPUS(ncpu)
set(CTEST_CMAKE_GENERATOR "Unix Makefiles")
set(CTEST_BUILD_COMMAND "make -k -j${ncpu} $ENV{TARGET}")

set(CTEST_BUILD_NAME $ENV{LCG_VERSION}-$ENV{PLATFORM})

#---{ Notes to be attached to the build }----------------------------------------------------------
set(CTEST_NOTES_FILES ${CTEST_NOTES_FILES} ${CTEST_SOURCE_DIRECTORY}/cmake/toolchain/heptools-$ENV{LCG_VERSION}.cmake)
set(CTEST_NOTES_FILES ${CTEST_NOTES_FILES} ${CTEST_BINARY_DIRECTORY}/dependencies.json)

#---{ CTest commands }-----------------------------------------------------------------------------
file(REMOVE_RECURSE ${CTEST_BINARY_DIRECTORY})

if("$ENV{CLEAN_INSTALLDIR}" STREQUAL "true")
    file(REMOVE_RECURSE ${CTEST_INSTALL_DIRECTORY})
endif()

if("$ENV{TEST_LABELS}" STREQUAL "")
    set(CTEST_LABELS Nightly)
else()
    set(CTEST_LABELS $ENV{TEST_LABELS})
endif()

set(ignore $ENV{LCG_IGNORE})
set(ignore "${ignore}")
set(options -DLCG_VERSION=$ENV{LCG_VERSION}
            -DCMAKE_INSTALL_PREFIX=${CTEST_INSTALL_DIRECTORY}
            -DPDFsets=$ENV{PDFSETS}
            -DLCG_INSTALL_PREFIX=$ENV{LCG_INSTALL_PREFIX}
            -DLCG_SAFE_INSTALL=ON
            -DLCG_IGNORE=${ignore}
            -DCMAKE_VERBOSE_MAKEFILE=OFF
            -DLCG_HOST_ARCH=$ENV{LCG_HOST_ARCH}
            -DLCG_HOST_OS=$ENV{LCG_HOST_OS}
            -DLCG_HOST_OSVERS=$ENV{LCG_HOST_OSVERS}
            -DLCG_HOST_COMP=$ENV{LCG_HOST_COMP}
            -DLCG_HOST_COMPVERS=$ENV{LCG_HOST_COMPVERS}
            $ENV{LCG_EXTRA_OPTIONS})
set(lcg_ignore ${})

#---{ The build mode drives the name of the slot in CDash }----------------------------------------
ctest_start($ENV{CTEST_TRACK} TRACK $ENV{CTEST_TRACK})

if(NOT "$ENV{LCG_VERSION}" STREQUAL "latest")
    ctest_update()
endif()

ctest_configure(BUILD   ${CTEST_BINARY_DIRECTORY} 
                SOURCE  ${CTEST_SOURCE_DIRECTORY}
                OPTIONS "${options}")
ctest_build(BUILD ${CTEST_BINARY_DIRECTORY})

if(NOT $ENV{CTEST_TRACK} STREQUAL "Release")
    ctest_test(PARALLEL_LEVEL ${ncpu} INCLUDE_LABEL "${CTEST_LABELS}")
endif()

file(STRINGS ${CTEST_BINARY_DIRECTORY}/fail-logs.txt logs)
ctest_upload(FILES ${logs})
ctest_submit()
