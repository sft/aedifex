#!/usr/bin/env bash

set +x # Disable debug print of each command
set -e # Enable failure of this script if any error occurs during a called command

# Small helper function to print an info message
function info_msg {
    echo
    echo "[INFO] $@"
}

# Small helper function to print error message and exit with a failure
function error_msg {
    echo
    echo "[ERROR] $@"
    echo
    exit 1
}


#---{ These environment variables must be set before calling this script }-------------------------
echo
echo "---[ Input Variables ]---------------------------"
echo "BUILD_TYPE:  ${BUILD_TYPE:?You need to set BUILD_TYPE (non-empty)}"
echo "COMPILER:    ${COMPILER:?You need to set COMPILER (non-empty)}"
echo "LCG_VERSION: ${LCG_VERSION:?You need to set LCG_VERSION (non-empty)}"


#---{ Set additional CMake parameters based on Jenkins vaiables }----------------------------------
if [[ -n ${INSTALL_SOURCES} ]] && ${INSTALL_SOURCES}
then
    LCG_EXTRA_OPTIONS="-DLCG_SOURCE_INSTALL=ON"
else
    LCG_EXTRA_OPTIONS="-DLCG_SOURCE_INSTALL=OFF"
fi

if [[ -n ${INSTALL_TARBALLS} ]] && ${INSTALL_TARBALLS}
then
    LCG_EXTRA_OPTIONS="${LCG_EXTRA_OPTIONS};-DLCG_TARBALL_INSTALL=ON"
else
    LCG_EXTRA_OPTIONS="${LCG_EXTRA_OPTIONS};-DLCG_TARBALL_INSTALL=OFF"
fi

if [[ -n ${SUPPRESS_DEV_WARNINGS} ]] && ${SUPPRESS_DEV_WARNINGS}
then
    LCG_EXTRA_OPTIONS="${LCG_EXTRA_OPTIONS};-Wno-dev"
fi

if [[ -n ${ADDITIONAL_CMAKE_OPTIONS} ]]
then
    LCG_EXTRA_OPTIONS="${LCG_EXTRA_OPTIONS};${ADDITIONAL_CMAKE_OPTIONS}"
fi


#---{ Determine the CDash track name ( required for CTest ) }--------------------------------------
if [[ -z ${LCG_VERSION} || ${LCG_VERSION} == exp* ]]
then
    CTEST_TRACK="Experimental"
elif [[ ${LCG_VERSION} == dev* ]]
then
    CTEST_TRACK="${LCG_VERSION}"
elif [[ ${LCG_VERSION} == root* ]]
then
    CTEST_TRACK="ROOT"
else
    CTEST_TRACK="Release"
fi


#---{ Determine the CMAKE_BUILD_TYPE as it is still used in LCG CMake }----------------------------
if [[ ${BUILD_TYPE} == opt ]]
then
    CMAKE_BUILD_TYPE="Release"
elif [[ ${BUILD_TYPE} == dbg ]]
then
    CMAKE_BUILD_TYPE="Debug"
else
    error_msg "There's only a configuration of build type 'opt' and 'dbg' but input was ${BUILD_TYPE}"
fi


#---{ Export a couple of default parameters for the build }----------------------------------------
export ARCHITECTURE="$( uname -m )"
export BUILD_TYPE=${BUILD_TYPE}
export CLEAN_INSTALLDIR="false"
export CMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
export COMPILER=${COMPILER}
export COPY_LOGS=${COPY_LOGS:=false}
export CTEST_SITE="cdash.cern.ch"
export CTEST_TRACK=${CTEST_TRACK}
export CVMFS_CONTRIB="/cvmfs/sft.cern.ch/lcg/contrib"
export EOS_MGM_URL="root://eosuser.cern.ch"
export GIT_HASH_AEDIFEX="$( cd /aedifex; git rev-parse HEAD )"
export GIT_HASH_LCGCMAKE="$( cd /lcgcmake; git rev-parse HEAD )"
export GIT_HASH_LCGCTEST="$( cd /lcgtest; git rev-parse HEAD )"
export LC_ALL="en_US.UTF-8"
export LC_CTYPE="en_US.UTF-8"
export LCG_EXTRA_OPTIONS=${LCG_EXTRA_OPTIONS}
export LCG_VERSION=${LCG_VERSION}
export PDFSETS="minimal"
export TARGET=${TARGET:="all"}
export TEST_LABELS=${TEST_LABELS:="Nightly|PhysicsCheck"}
export VERSION=${VERSION:="trunk"}
export WEEKDAY_NAME=$( date +%a )


#---{ Get preliminary platform desription }--------------------------------------------------------
PLATFORM=$( python3 /aedifex/build/get_platform.py )


#---{ Setup CMake version 3+ (the system version is often very old) }------------------------------
if [[ ${PLATFORM} == *slc6* || ${PLATFORM} == *cc7* || ${PLATFORM} == *centos7* || ${PLATFORM} == *ubuntu* ]]
then
    if [[ ${ARCHITECTURE} == *x86_64* ]]
    then
        # Using the 'latest' symbolic link to the latest CMake version
        export PATH=${CVMFS_CONTRIB}/CMake/latest/Linux-x86_64/bin:${PATH}
    elif [[ ${ARCHITECTURE} == *i686* || ${ARCHITECTURE} == *i386* ]]
    then
        # 3.3.2 is the last 32bit version of CMake we provide
        export PATH=${CVMFS_CONTRIB}/CMake/3.3.2/Linux-i686/bin:${PATH}
    elif [[ ${ARCHITECTURE} == *aarch64* ]]
    then
        # TODO: Test
        # ARM builds use the cmake3 binary from the system
        alias cmake="cmake3"
        alias ctest="ctest3"
    else
        error_msg "There's no CMake 3 version fitting for this architecture: ${ARCHITECTURE}"
    fi
fi
echo
echo "---[ CMake Version ]-----------------------------"
cmake --version | head -n 1
ctest --version | head -n 1


#---{ Set internal LABEL_COMPILER variable to be used in contrib paths }---------------------------
if [[ ${PLATFORM} == *slc6* ]]
then
    LABEL_COMPILER=slc6
elif [[ ${PLATFORM} == *cc7* || ${PLATFORM} == *centos7* ]]
then
    LABEL_COMPILER=centos7
fi


#---{ Setup ccache for GCC C and C++ compilers - no support for Clang or Fortran }-----------------
function setup_ccache {

    export CCACHE_PATH=${PATH}
    export CCACHE_BASEDIR=${WORKSPACE}

    if [[ $( which gcc ) == /usr/local/bin/* ]]
    then
        info_msg "ccache is already in use (probably a native GCC), skipping ccache setup ..."
    else
        info_msg "Setup ccache ..."

        export CCACHE_CC=${CC}
        export PATH=/usr/local/bin:${PATH}

        local CCACHE_EXECUTABLE=$( which ccache )

        # Set C and C++ compiler to ccache
        #export CC="${CCACHE_EXECUTABLE} ${CC}"
        #export CXX="${CCACHE_EXECUTABLE} ${CXX}"
        #export FC="${CCACHE_EXECUTABLE} ${FC}"
        echo "aedifex before CC:  ${CC}"
        echo "aedifex before CXX: ${CXX}"
        echo "aedifex before FC:  ${FC}"
        #export CC="$( which gcc )"
        # alias g++="ccache gcc -xc++ -lstdc++ -shared-libgcc"
        # export CXX="/usr/local/bin/ccache gcc -xc++ -lstdc++ -shared-libgcc"
        #export FC="$( which gfortran )"
        echo "aedifex after CC:  ${CC}"
        echo "aedifex after CXX: ${CXX}"
        echo "aedifex after FC:  ${FC}"
    fi

    echo
    ccache --version | head -n 1
    echo "---[ ccache Statistics ]------------------"
    ccache -s
    echo "---[ ccache Configuration ]---------------"
    ccache -p
}


#---{ Setup the compiler (GCC, Clang or native) }--------------------------------------------------
if [[ ${COMPILER} == *gcc* ]]
then
    if [[ ${PLATFORM} == *slc6* || ${PLATFORM} == *cc7* || ${PLATFORM} == *centos7* ]]
    then
        case ${COMPILER} in
            gcc61 | gcc61abi4 | gcc62 | gcc62abi4 | gcc62binutils | gcc63 )
                # Remove leading 'gcc', add a dot after the first letter
                GCC_VERSION="${COMPILER:3:1}.${COMPILER:4}"
                ;;
            gcc7 | gcc7binutils | gcc8 | gcc8binutils | gcc8testing | gcc9 | gcc9binutils | gcc9testing )
                # Remove the leading 'gcc' part
                GCC_VERSION="${COMPILER:3}"
                ;;
            *)
                error_msg "There's no configuration for this gcc compiler: ${COMPILER}"
                ;;
        esac

        if [[ ${ARCHITECTURE} == *aarch64* ]]
        then
            # TODO: Arch location
            # Use local compiler installed with 
            source /opt/
        else
            source ${CVMFS_CONTRIB}/gcc/${GCC_VERSION}/${ARCHITECTURE}-${LABEL_COMPILER}/setup.sh
        fi
        
        export CC=$( which gcc )
        export CXX=$( which g++ )
        export FC=$( which gfortran )

    elif [[ ${PLATFORM} == *ubuntu* ]]
    then
        if [[ ${COMPILER} == *gcc8* ]]
        then
            # Special case: Setup gcc8 from the official package repository on Ubuntu 18.04
            export CC=$( which gcc-8 )
            export CXX=$( which g++-8 )
            export FC=$( which gfortran-8 )
        else
            error_msg "The only supported GCC compiler in Ubuntu (apart from native) is version 8"
        fi
    else
        error_msg "GCC is only configured for SLC6, CentOS7 and Ubuntu"
    fi

    # Setup ccache for gcc and g++
    setup_ccache

    echo
    echo "---[ GCC Version ]------------------------"
    ${CC} -v
    echo
    echo "---[ G++ Version ]------------------------"
    ${CXX} -v

elif [[ $COMPILER == *clang* ]]
then
    case ${COMPILER} in
        clang501 | clang501binutils | clang600 | clang600binutils | clang700binutils | clang800binutils )
            # Remove 'clang' prefix and insert a dot after the first and the second letter
            CLANG_VERSION="${COMPILER:5:1}.${COMPILER:6:1}.${COMPILER:7}"
            ;;
        clang7 | clang8 )
            # Remove 'clang' part and append two zeros for minor and patch version
            CLANG_VERSION="${COMPILER:5:1}.0.0"
            ;;
        *)
            error_msg "There's no configuration for this clang compiler: ${COMPILER}"
            ;;
    esac

    COMPILER_SETUP=${CVMFS_CONTRIB}/clang/${CLANG_VERSION}/${ARCHITECTURE}-${LABEL_COMPILER}/setup.sh

    if [[ ! -r ${COMPILER_SETUP} ]]
    then
        # Use the llvm contribs as fallback for clang (clang only provides the latest versions)
        COMPILER_SETUP=${CVMFS_CONTRIB}/llvm/${CLANG_VERSION}/${ARCHITECTURE}-${LABEL_COMPILER}/setup.sh
    fi

    source ${COMPILER_SETUP}
    export CC=$( which clang )
    export CXX=$( which clang++ )
    export FC=$( which gfortran )

    echo
    echo "---[ Clang Version ]----------------------"
    ${CC} -v

elif [[ ${PLATFORM} == *native* ]]
then
    if [[ ${PLATFORM} == *mac* ]]
    then
        # OS default compiler on macOS
        export LIBRARY_PATH=/usr/local/gfortran/lib
        export PATH=${PATH}:/usr/local/bin:/opt/X11/bin
        export CC=$( which clang )
        export CXX=$( which clang++ )
        export FC=$( which gfortran )

        echo
        echo "---[ Native Compiler Version ]------------"
        ${CC} -v
    else
        # OS default compiler, e.g. used on Ubuntu and Fedora
        export CC=$( which gcc )
        export CXX=$( which g++ )
        export FC=$( which gfortran )

        # Setup ccache for native gcc and g++
        setup_ccache

        echo
        echo "---[ Native Compiler Version ]------------"
        ${CC} -v
    fi
fi


#---{ Recalculate PLATFORM after the compiler has been properly setup }----------------------------
export $( python3 /aedifex/build/get_platform.py bash )

#---{ Overwrite platform in heptools-common.cmake in the sft/lcgcmake project }--------------------
export BINARY_TAG=${PLATFORM}


#---{ Go compiler - only centos7 and slc6 supported }----------------------------------------------
CVMFS_LATEST_GO="${CVMFS_CONTRIB}/go/latest/x86_64-${LABEL_COMPILER}"
if [[ $PLATFORM == *slc6* || $PLATFORM == *cc7* || $PLATFORM == *centos7* ]] && [[ -r ${CVMFS_LATEST_GO} ]]
then
    export PATH=${CVMFS_LATEST_GO}/bin:${PATH}
    export GOROOT_BOOTSTRAP=${CVMFS_LATEST_GO}
fi
