#!/usr/bin/env bash

set +x # Disable debug print of each command
set -e # Enable failure of this script if any error occurs during a called command

#---{ Set lots of variables for the build, setup compiler, update PLATFORM }-----------------------
source /aedifex/build/setup_environment.sh


#---{ Cleanup build indicators and the workspace from previous builds }----------------------------
rm -rf ${WORKSPACE}/build
rm -rf ${WORKSPACE}/install
rm -rf ${WORKSPACE}/packaging
rm -fv ${WORKSPACE}/build/isDone-${PLATFORM}
rm -fv ${WORKSPACE}/build/isDone-unstable-${PLATFORM}


# TODO: Only used in cleanbuildmachine.py -> Use in actual Jenkins clean jobs as well
#---{ Mark this directory as "in use" to prevent clean-up by Jenkins }-----------------------------
touch ${WORKSPACE}/controlfile


#---{ Prepare a script to run the Docker container in the exact same state as the build }----------
ESCAPED_SOURCE_COMMAND='source /aedifex/build/setup_environment.sh'

cat << EOF > "${WORKSPACE}/environment.sh"
#!/usr/bin/env bash
set +x
#--- Checkout the correct git state -----------------------
\$( cd /aedifex;  git checkout ${GIT_HASH_AEDIFEX}  )
\$( cd /lcgcmake; git checkout ${GIT_HASH_LCGCMAKE} )
\$( cd /lcgtest;  git checkout ${GIT_HASH_LCGTEST}  )
#--- Set input for build-environment.sh -------------------
export BUILD_TYPE=${BUILD_TYPE}
export COMPILER=${COMPILER}
export LCG_VERSION=${LCG_VERSION}
#--- Load all variables for the build environment ---------
${ESCAPED_SOURCE_COMMAND}
EOF


#---{ Print environment vaiables }-----------------------------------------------------------------
echo
echo "---[ Environment Variables ]--------------"
env | sort | sed 's/:/:?     /g' | tr '?' '\n'
echo "------------------------------------------"
echo

#---{ Run the LCG CMake build }--------------------------------------------------------------------
ctest --extra-verbose --script /aedifex/build/lcgcmake.cmake 
BUILD_RESULT=$?


#---{ Create a list of all successfully built packages; used e.g. in devgeantv }-------------------
TIMESTAMP="$( date +%Y-%m-%d--%H:%M:%S )"
PACKAGES_LIST="${WORKSPACE}/build/LCG_${LCG_VERSION}_${PLATFORM}.txt"
SUCCESFULL_PACKAGES="${WORKSPACE}/build/succesfull_pkgs"

printf > ${SUCCESFULL_PACKAGES} "%s%s\n" \
       "# ${TIMESTAMP} PKGS_OK " \
       "$( ls ${WORKSPACE}/build/timestamps/*-stop.timestamp | rev | cut -d '-' -f 2- | cut -d '/' -f 1 | rev | tr '\n' ' ' )"

printf > ${PACKAGES_LIST} "%s\n%s" "$( cat ${SUCCESFULL_PACKAGES} )" "$( cat ${PACKAGES_LIST} )"


#---{ Create a indicator file that informs about the build result }--------------------------------
if [[ ${BUILD_RESULT} -eq 0 ]]
then
    info_msg "The build was successful -> Creating 'isDone-${PLATFORM}' ..."
    touch ${WORKSPACE}/build/isDone-${PLATFORM}
else
    echo
    echo "[ERROR] The build has failed -> Creating 'isDone-unstable-${PLATFORM}' ..."
    touch ${WORKSPACE}/build/isDone-unstable-${PLATFORM}
fi


#---{ Copy the build logs to a common space for further processing }-------------------------------
if ${COPY_LOGS}
then
    info_msg "Copying all build logs to ${WORKSPACE}/logs ..."
    rm -rfv ${WORKSPACE}/logs
    mkdir ${WORKSPACE}/logs
    find ${WORKSPACE}/build/{externals,generators,projects,pyexternals} -maxdepth 4 -iname "*.log" -exec cp {} ${WORKSPACE}/logs/ \;
fi


#---{ Collect and export ENV variables required by the test }--------------------------------------
CTEST_TIMESTAMP=$( head -1 ${WORKSPACE}/build/Testing/TAG )
CTEST_TAG=$( tail -1 ${WORKSPACE}/build/Testing/TAG )
BUILD_HOSTNAME=$( hostname )
OS_VERSION=$( echo ${PLATFORM} | cut -d- -f2 )

info_msg "Exporting build properties for following builds to ${WORKSPACE}/properties.txt ..."
cat << EOF > ${WORKSPACE}/properties.txt
BUILD_HOSTNAME=${BUILD_HOSTNAME}
BUILD_TYPE=${BUILD_TYPE}
COMPILER=${COMPILER}
CTEST_TAG=${CTEST_TAG}
CTEST_TIMESTAMP=${CTEST_TIMESTAMP}
CTEST_TRACK=${CTEST_TRACK}
LABEL=${LABEL}
OS_VERSION=${OS_VERSION}
PLATFORM=${PLATFORM}
WEEKDAY_NAME=${WEEKDAY_NAME}
EOF


exit ${BUILD_RESULT}
